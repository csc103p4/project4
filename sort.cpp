#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <iostream>
#include <set>
#include <vector>
#include <strings.h>
#include <algorithm>
using std::string;
using std::cin;
using std::cout;
using std::set;
using std::multiset;
using std::endl;
using std::vector;

struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */

void descend();
void caseSensitive();
void noDuplicate();

void descend() {

	string words;
	vector<string> v; // vector that holds strings
	while (getline(cin,words)) v.push_back(words);

	sort(v.begin(),v.end());

		for ( int i = v.size()-1; i != (-1) ; i--)
		cout << v[i] << "\n";
}

void caseSensitive() {
  #if 1
	string words;
	vector<string> v;
	while (getline(cin,words)) v.push_back(words);
	sort(v.begin(), v.end(), igncaseComp());

		for ( vector<string>::iterator i = v.begin() ; i != v.end(); i++) {
			cout << (*i) << "\n";
		}
	#endif
}

void noDuplicate() {
	string words;
	set<string> tempSet;
	vector<string> v;
	while (getline(cin,words)) v.push_back(words);

		for	(size_t i = 0; i < v.size() ; i++ ) {

				tempSet.insert(v[i]);
		}

		v.erase(v.begin(), v.end());

		for ( set<string>::iterator i = tempSet.begin(); i != tempSet.end(); i++ ) {

			cout << (*i) << "\n";
		}
}



static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";


int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	if (descending){
		descend();
	}
	else if (ignorecase){
		caseSensitive();
	}
	else if (unique){
		noDuplicate();
	}
	else {
		printf(usage,argv[0]);
	}
	/* TODO: write me... */

	return 0;
}

// Hi :D
