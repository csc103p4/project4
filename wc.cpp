/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */
#include <iostream>
#include <string>
#include <set>
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <set>
#include <string>
#include <vector>
#include <algorithm>
using std::set;
using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::vector;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}


int countcha(string str){
	int count = str.length()+1;
	return count;
}
int countword(const string& s, set<string>& wl){
	int count = 0;
	string word;
	for(int i=0; i<s.length();i++){
		if(isalpha(s[i])){
			word = word + s[i];
		}
		if(i == s.length()-1 && isalpha(s[i])){
			count++;
			wl.insert(word);
			word="";
		}
		else if(isalpha(s[i]) && isspace(s[i+1])){
			count++;
			wl.insert(word);
			word="";
		}
	}
	return count;
}
int main()
{
	string line;
	set<string> wordlist;
	set<string> word;
	int countword(const string& s, set<string>& wl);
	int cL=0;
	int cW=0;
	int cB=0;
	int cUW=0;
	while(getline(cin,line)){
		if(line == "stop") break;  //this won't count stop as a word
		word.insert(line);
		cL++;
		cB=countcha(line)+cB;
		cW=countword(line, wordlist)+cW;
	}
		for(set<string>::iterator i=wordlist.begin();i!=wordlist.end();i++)
		{
			cUW++;
		}
		cout<<cL<<"\t"<<cW<<"\t"<<cB<<"\t"<<cUW;
		cout << endl;
			return 0;
}
